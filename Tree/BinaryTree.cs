﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    public class TreeNode {
        private int info;      // информационное поле  
        private TreeNode left;  // ссылка на левое поддерево  
        private TreeNode right; // ссылка на правое поддерево  

        public int Info { get; set; }    // свойства  
        public TreeNode Left { get; set; }
        public TreeNode Right { get; set; }

        public TreeNode() { }   // конструкторы  
        public TreeNode(int info)
        {
            Info = info;
        }
        public TreeNode(int info, TreeNode left, TreeNode right)
        {
            Info = info; Left = left; Right = right;
        }
    }
    public class BinaryTree
    {
        public int n = 0; // переменная для подсчета суммы
        public int pos; // переменная для количества положительных чисел в дереве
        public int negativ; // переменная для количества отрицательных чисел в дереве
        public int find; // переменная для количества совпадений в дереве
        private TreeNode root;  // ссылка на корень дерева  
        

        public TreeNode Root    // свойство, открывающее доступ к корню дерева 
        {
            get { return root; }
            set { root = value; }
        }

     
        public BinaryTree() // создание пустого дерева  
        {
            root = null;
        }


       /// Метод построения сбалансированного дерева из N узлов 
              public TreeNode Create_Balanced(int n)  	// n – количество узлов в дереве  
        {

            int x;
            TreeNode root;    // ссылка на корень дерева и на корень любого из поддеревьев 
            if (n == 0)
                root = null; // если n == 0, построить пустое дерево
            else
            {   // заполнить информационное поле корня  
                Console.WriteLine("Введите значение поля узла (число):");
                x = Convert.ToInt32(Console.ReadLine()); // пользовательский ввод
                root = new TreeNode(x);     // создать корень дерева  
                root.Left = Create_Balanced(n / 2);    // построить левое поддерево (*1*)
                root.Right = Create_Balanced(n - n / 2 - 1);    // построить правое поддерево  (*2*)
            }
            return root; //(*3*)
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        public void KLP(TreeNode root) // root – ссылка на корень дерева и любого из поддеревьев  
        {
            if (root != null)    // дерево не пусто?  
            {   // распечатать информ. поле корневого узла  
                Console.Write(root.Info+ " ");
                KLP(root.Left);     // (*1*)обойти левое поддерево в нисходящем порядке  
                KLP(root.Right);    // (*2*)обойти правое поддерево в нисходящем порядке  
            }
            //(* 3 *) 
        }

        public int AVG(TreeNode root) {

            if (root != null)    // дерево не пусто?  
            {   
                n += root.Info; // суммирование
               
                AVG(root.Left);     // (*1*)обойти левое поддерево в нисходящем порядке  
                AVG(root.Right);    // (*2*)обойти правое поддерево в нисходящем порядке  
            }
            return n; // вернуть сумму в main
        }

        public string Znak(TreeNode root)
        {

            if (root != null)    // дерево не пусто?  
            {  // подсчет положительных и отрицательных значений
                if (root.Info > 0) { pos++; }

                else { negativ++; }
              
                Znak(root.Left);     // (*1*)обойти левое поддерево в нисходящем порядке  
                Znak(root.Right);    // (*2*)обойти правое поддерево в нисходящем порядке  
            }
           return "Количество отрицательных: " + negativ.ToString() + "\nКоличество положительных: " + pos.ToString();
           
        } 

        public int Find(TreeNode root,int t)
        {

            if (root != null)    // дерево не пусто?  
            {
                if (root.Info == t)
                {
                    find += 1; // совпадения
                  
                }
                Find(root.Left, t);     // (*1*)обойти левое поддерево в нисходящем порядке  
                Find(root.Right, t);    // (*2*)обойти правое поддерево в нисходящем порядке  
             }
             return find;
        }

    }
}
