﻿using System;

namespace Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree T = new BinaryTree();    // объявление объекта класса “Бинарное дерево” 
            Console.WriteLine("Введите количество узлов: ");
            int n = Convert.ToInt32(Console.ReadLine());
            T.Root = T.Create_Balanced(n);  // создание сбалансированного дерева из n узлов  
            T.KLP(T.Root); 	// трасса нисходящего обхода дерева  
            Console.WriteLine("\nCреднее арифметическое значений информационных полей узлов дерева = "+ T.AVG(T.Root)/n); //  среднее арифметическое  
            Console.WriteLine(T.Znak(T.Root)); //  количество узлов дерева с положительными и отрицательными значениями
            Console.WriteLine("Введите число для поиска: ");
            int t = Convert.ToInt32(Console.ReadLine()); // чтение
            Console.WriteLine("Количество совпадений:  " + T.Find(T.Root,t));
           
        
        }
    }
}
